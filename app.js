const e = require("express");
const express = require("express");
const fs = require("fs");
const path = require("path");
const app = express();
const urlencodedParser = express.urlencoded({ extended: false });
const jsonParser = express.json();

function writeLogsFile(fd) {
  fs.appendFileSync(__dirname + "/example/logs.txt", fd);
}

app.post("/api/files", jsonParser, function (request, response) {
  writeLogsFile(`${request.method} ${new Date().toLocaleDateString()}`);

  if (!request.body.content)
    return response
      .status(400)
      .send({ message: "Please specify 'content' parameter" });

  if (!request.body.filename)
    return response
      .status(400)
      .send({ message: "Please specify 'filename' parameter" });

  const pathDirectory = `${__dirname}/app/files`;
  const pathFile = `${__dirname}/app/files/${request.body.filename}`;

  if (!fs.existsSync("./app/files")) {
    fs.mkdirSync("./app/files", { recursive: true });
  }

  if (!fs.existsSync(pathFile)) {
    fs.appendFileSync(pathFile, request.body.content);
    response.status(200).send({ message: "File created successfully" });
  } else {
    response.status(500).send({ message: "Server error" });
  }
});

app.get("/api/files", function (request, response) {
  writeLogsFile(`${request.method} ${new Date()}`);

  const existFile = fs.readdirSync(`${__dirname}/app/files`);

  if (existFile.length) {
    response.status(200).send({
      message: "Success",
      files: existFile,
    });
  } else {
    response.status(400).send({ message: "Server error" });
  }
});

app.get("/api/files/:filename", function (request, response) {
  writeLogsFile(`${request.method} ${new Date()}`);

  const file = request.params.filename;
  const pathFile = `${__dirname}/app/files/${request.params.filename}`;

  if (fs.existsSync(pathFile)) {
    const text = fs.readFileSync(pathFile, "utf-8");
    const extension = path.extname(pathFile);
    const result = extension.match(/\w+/);
    fs.stat(pathFile, (error, stat) => {
      if (error) {
        response
          .status(400)
          .send({ message: `No file with '${file}' filename found` });
      } else {
        response.status(200).send({
          message: "Success",
          filename: `${file}`,
          content: `${text}`,
          extension: `${result}`,
          uploadedDate: `${stat.ctime.toLocaleTimeString()}`,
        });
      }
    });
  } else {
    response
      .status(400)
      .send({ message: `No file with '${file}' filename found` });
  }
});

app.listen(8080, () => console.log("Server is starting on 8080 port"));
